import { NgxUploaderPage } from './app.po';

describe('ngx-uploader App', () => {
  let page: NgxUploaderPage;

  beforeEach(() => {
    page = new NgxUploaderPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
